# react-quiz-app

## Table of Contents

[[_TOC_]]

## Development Setup

### Dependencies

This project was built with Node `v18.17.0`, which is currently the most recent LTS release. You should ensure this is the version of Node you are using for project work:

```sh
$ node -v
v18.17.0
$ npm -v
9.6.7
```

If a newer LTS release becomes available and is confirmed to work with this application, the project Node version should be updated accordingly.

With Node installed, you can use npm to install project dependencies locally:

```sh
npm install
```

### Starting the Application

The application can be started locally with:

```sh
npm run dev
```

With HMR enabled, you should be able to view changes made to the web app in real time. To take advantage of this, it is recommended that you leave this script running in a terminal instance.

With dependencies installed and the application running, you should now be ready to start project work.

## Linting

### Running the Linter

Project linting is done through eslint which can be invoked with:

```sh
npm run lint
```

The linter is configured to enforce the [Airbnb JavaScript Style Guide](https://airbnb.io/javascript/react/).

For simpler formatting errors, you can attempt to let eslint correct them automatically with:

```sh
npm run lint-fix
```

Running the normal `lint` script instead of `lint-fix` is preferred so that you can view all errors before attempting to auto-fix.

The pipeline for this project has a lint job as part of the test stage. Because of this, it's recommended that you lint your code changes locally before pushing them. This eliminates the need to push extra changes when the pipeline lint job fails.

### Linter Rules Reference

During linting, each error will reference a rule. Searching for this rule in the docs linked below will provide you with examples of rule-breaking code and how it can be fixed.

[Standard eslint rules](https://eslint.org/docs/latest/rules/)

[React eslint rules](https://github.com/jsx-eslint/eslint-plugin-react/tree/master/docs/rules)

## Component Testing

### Running Tests

Component tests can be ran with:

```sh
npm run test
```

The pipeline for this project has a test job as part of the test stage. Because of this, it's recommended that you test your code changes locally before pushing them. This eliminates the need to push extra changes when the pipeline test job fails.

### Writing Tests

TODO: Add component testing to readme (GitLab #17)
