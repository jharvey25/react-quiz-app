export const dummyQuiz = {
  questions: [
    {
      question:
        'Dummy question 1',
      choices: [
        'A',
        'B',
        'C',
      ],
      correctAnswer: 'A',
    },
    {
      question:
        'Dummy question 2',
      choices: [
        'A',
        'B',
        'C',
      ],
      correctAnswer: 'B',
    },
    {
      question:
        'Dummy question 3',
      choices: [
        'A',
        'B',
        'C',
      ],
      correctAnswer: 'C',
    },
  ],
};

export const resultInitialState = {
  score: 0,
  correctAnswers: 0,
  wrongAnswers: 0,
};
