import React from 'react';
import Quiz from './components/Quiz/Quiz';
import { dummyQuiz } from './constants';

function App() {
  return <Quiz questions={dummyQuiz.questions} />;
}

export default App;
