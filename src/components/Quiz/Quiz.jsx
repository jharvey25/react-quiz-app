import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { resultInitialState } from '../../constants';
import './Quiz.scss';

function Quiz({ questions }) {
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [answerIndex, setAnswerIndex] = useState(null);
  const [answer, setAnswer] = useState(null);
  const [result, setResult] = useState(resultInitialState);
  const [showResult, setShowResult] = useState(false);

  const { question, choices, correctAnswer } = questions[currentQuestion];

  const onClickAnswer = (answer, index) => {
    setAnswerIndex(index);
    if (answer === correctAnswer) {
      setAnswer(true);
    } else {
      setAnswer(false);
    }
  };

  const onClickNext = () => {
    setAnswerIndex(null);
    setResult((prev) => (answer
      ? {
        ...prev,
        score: prev.score + 5,
        correctAnswers: prev.correctAnswers + 1,
      } : {
        ...prev,
        wrongAnswers: prev.wrongAnswers + 1,
      }));

    // Logic for reaching final question of quiz
    if (currentQuestion !== questions.length - 1) {
      setCurrentQuestion((prev) => prev + 1);
    } else {
      setCurrentQuestion(0);
      setShowResult(true);
    }
  };

  const onClickTryAgain = () => {
    setResult(resultInitialState);
    setShowResult(false);
    // NOTE: setCurrentQuestion is already reset to 0 in onClickNext()
  };

  return (
    <div className="quiz-container">
      {!showResult ? (
        <>
          <span className="active-question-num">{currentQuestion + 1}</span>
          <span className="total-questions">
            /
            {questions.length}
          </span>
          <h2>{question}</h2>
          <ul>
            {
            choices.map((answer, index) => (
              <li
                onClick={() => onClickAnswer(answer, index)}
                key={answer}
                className={answerIndex === index ? 'selected-answer' : null}
              >
                {answer}
              </li>
            ))
          }
          </ul>
          <div className="footer">
            <button type="button" onClick={() => onClickNext()} disabled={answerIndex === null}>
              {currentQuestion === questions.length - 1 ? 'Finish' : 'Next'}
            </button>
          </div>
        </>
      ) : (
        <div className="result">
          <h3>Result</h3>
          <p>
            Total Questions:
            {' '}
            <span>{questions.length}</span>
          </p>
          <p>
            Total Score:
            {' '}
            <span>{result.score}</span>
          </p>
          <p>
            Correct Answers:
            {' '}
            <span>{result.correctAnswers}</span>
          </p>
          <p>
            Wrong Answers:
            {' '}
            <span>{result.wrongAnswers}</span>
          </p>
          <button type="button" onClick={() => onClickTryAgain()}>Try again</button>
        </div>
      )}
    </div>
  );
}

Quiz.propTypes = {
  questions: PropTypes.arrayOf(PropTypes.question).isRequired,
};

export default Quiz;
